#!/usr/bin/env bash

# Install base dependencies
/usr/bin/env DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::='--force-confdef' install git build-essential vim zsh sanoid pv mbuffer tmux zip vim-nox wget whois iotop iftop

#curl https://danielbodnar.keybase.pub/.ssh/authorized_keys >> /root/.ssh/authorized_keys

curl -O https://gitlab.com/bitbuilder-cloud/init/-/raw/main/pve/xs-install-post.env
curl -O https://gitlab.com/bitbuilder-cloud/init/-/raw/main/pve/install-post.sh

bash install-post.sh

echo "Install ohmyzsh:"
echo 'sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"'
echo "Change theme:"
echo "sed -i'' 's/robbyrussell/rkj-repos/g'"
